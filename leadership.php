<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Leadership at Marlboro Montessori Camp</title>
</head>
<body>
<div align="center">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent">
	<img src="images/leader_topimg.jpg" alt="Leadership school" />
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable">
		<tr valign="top">
			<td width="450">
			<img src="images/leader_hdr.gif"  alt="Students school leadership" class="hdrimg"/>
			<div class="text"><?php include("text/mmc_leader.txt"); ?></div>	
			<img src="images/leader_img1_lft.jpg"  alt="Leadership School Prep" class="sectionImgs" />
			</td>
			<td width="416">
			<img src="images/leader_img1_rt.jpg"  alt="Leadership pre schools" class="sectionImgs" />
			</td>
		</tr>
	</table></div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
