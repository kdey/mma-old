<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Leadership Academy for elementary students at Marlboro Montessori, NJ</title>
<meta name="description" content="The Montessori Leadership Academy in NJ near Matawan for elementary students is dedicated to enhance and further develop leadership quality of children."/>
<meta name="keywords" content="Day Care, Montessorie's leadership academy, Matawam Montessori Leadership academy, children's leadership academy, School, Summer Camp, Preschool, Montessori schools in Marlboro, Montessori schools in Matawan, Admission in Montessori schools in Marlboro, Montessori school in Wickatunk, private schools in Marlboro, N.J.,private schools in Monmouth county, Montessori Learning Center Matawan, Montessori learning materials, Montessori teaching materials, Montessori Advantage Learning Center, elementary schools in New Jersey,  Montessori elementary schools in Monmouth county NJ,  IQ, intelligent children, gifted programs for children in NJ, gifted students, tutoring and enrichment, Montessori Tutor, Montessori afterschool tutoring,  Montessori afterschool learning clubs, tutoring for elementary schools"/>
</head>
<body>
<div align="center" itemscope itemtype="http://schema.org/Preschool">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent">
	<img src="images/leader_topimg.jpg" alt="Marlboro Montessori Leadership Academy"/>
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable">
		<tr valign="top">
			<td width="481">
			<img src="images/leader_hdr.gif"  class="hdrimg" alt="Multiple Intelligence"/>
			<div class="text"><?php include("text/mm_leadership.txt"); ?></div>	
			<img src="images/leader_img1_lft.jpg"  class="sectionImgs" alt="Multiple Intelligences"/>
			<div class="text"><?php include("text/mm_leadership2.txt"); ?></div>	
			</td>
			<td width="381">
			<img src="images/leader_img1_rt.jpg"  alt="Critical Thinking Activities" class="sectionImgs" />
			<div class="text"><?php include("text/mm_leadership_rtclm.txt"); ?></div>	
			<img src="images/leader_img2_rt.jpg"  class="sectionImgs" alt="Problem Solving Thinking"/>

			</td>
		</tr>
	</table></div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
