<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Child Care, Day Care preschool in Wickatunk, NJ | Marlboro Montessori</title>
<meta name="description" content="Preschool at Marlboro Montessori Academy located in Wickatunk, NJ works to ensure all children have the early experiences necessary to be successful learners."/>
<meta name="keywords" content="Marlboro Montessori Academy,New Jersey Montessori Academy, Matawan Montessori, NJ Preschool, Preschool in Old bridge, preschool curriculum, preschools, preschool programs, preschool education, preschool learning games, preschool math games, private preschool, preschools in Morganville, montessori preschool curriculum, preschool summer camp, preschool in NJ, NJ Montessori school, montessori school morganville nj, Day care school in New Jersey, Day Care school in nj, Montessori Academy in Monmouth County, Montessori Learning Center, Montessori learning materials, Montessori teaching materials, Montessori school in Wickatunk,private schools in Marlboro, Day Care, private schools in Monmouth county, Montessori child care, Montessori day care, Summer Camp, Day Camp, Ney Jersey, Monmouth County"/>
</head>
<body>
<div align="center" itemscope itemtype="http://schema.org/Preschool">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent">
	<img src="images/preschool_topimg.jpg" alt="Preschool in NJ " />
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable">
		<tr valign="top">
			<td width="450">
			<img src="images/preschool_hdr.gif"  class="hdrimg" alt="Preschool Program for Children"/>
			<div class="text"><?php include("text/mm_preschool.txt"); ?></div>	
			<img src="images/preschool_img1_lft.jpg"  class="sectionImgs" alt="Montessori teaching materials" />
			<img src="images/preschool_img2_lft.jpg"  class="sectionImgs"  alt="Montessori learning materials"/>

			</td>
			<td width="417">
			<img src="images/preschool_img1_rt.jpg"  class="sectionImgs" alt="Montessori Preschool Classroom" />
			<img src="images/preschool_img2_rt.jpg"  class="sectionImgs" alt="preschool learning games" />
			<img src="images/preschool_img3_rt.jpg"  class="sectionImgs" alt="preschool curriculum" />

			</td>
		</tr>
	</table></div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
