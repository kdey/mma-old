<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Montessori Advantage at Marlboro Montessori Academy</title>
</head>
<body>
<div align="center">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent" style="background-color:#caebfa">
	<img src="images/advantage_topimg.jpg"  alt="Division Multiplication"/>
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable">
		<tr valign="top">
			<td width="448">
			<img src="images/advantage_hdr1.gif"  class="hdrimg" alt="The division"/>
			<div class="text"><?php include("text/mm_advantage1.txt"); ?></div>	
			<img src="images/advantage_img1_lft.jpg"  class="sectionImgs" alt="Long Division"/>

			<a name="additionboard"><img src="images/advantage_hdr2.gif"  class="hdrimg" alt="Addition Subtration"/></a>
			<div class="text"><?php include("text/mm_advantage2.txt"); ?></div>	

			<a name="subtractionboard"><img src="images/advantage_hdr3.gif"  alt="Division Worksheets" class="hdrimg"/></a>
			<div class="text"><?php include("text/mm_advantage7.txt"); ?></div>	
			<form name="_xclick" target="paypal" action="https://www.paypal.com" method="post">
			<input name="cmd" value="_cart" type="hidden">
			<input name="business" value="marlmont@aol.com" type="hidden">
			<input name="currency_code" value="USD" type="hidden">
			<input name="item_name" value="Addition Subtraction Board Set - Montessori Advantage" type="hidden">
			<input name="item_number" value="ADD_SUB_SET" type="hidden">
			<input name="amount" value="56.00" type="hidden">
			<input name="shipping" value="15.95" type="hidden">
			<input name="shipping2" value="15.95" type="hidden">
			<input src="images/advantage_btn1.gif" name="submit" alt="Purchase Item..." border="0" type="image">
			<input name="add" value="1" type="hidden">
			</form>
			<img src="images/advantage_img2_lft.jpg"  alt="Montessori Materials" class="sectionImgs" />

			</td>
			<td width="399">
			<img src="images/advantage_img1_rt.jpg"  alt="Division Problems" class="sectionImgs" /><!-- 
			<img src="images/advantage_img2_btn.jpg"  class="sectionImgs" alt=""/> -->
			<a name="sandpapernumbers"><img src="images/advantage_hdr4.gif"  alt="Sandpaper Numbers" class="hdrimg"/></a>
			<div class="text"><?php include("text/mm_advantage3.txt"); ?></div>	
			<form name="_xclick" target="paypal" action="https://www.paypal.com" method="post">
			<input name="cmd" value="_cart" type="hidden">
			<input name="business" value="marlmont@aol.com" type="hidden">
			<input name="currency_code" value="USD" type="hidden">
			<input name="item_name" value="Sandpaper Numbers - Montessori Advantage" type="hidden">
			<input name="item_number" value="SAND" type="hidden">
			<input name="amount" value="20" type="hidden">
			<input name="shipping" value="6.95" type="hidden">
			<input src="images/advantage_btn2.gif" name="submit" alt="Purchase Item..." border="0" type="image">
			<input name="add" value="1" type="hidden">
			</form>

			<img src="images/advantage_img2_rt.jpg"  alt="Teaching Division" class="sectionImgs" />

			<a name="multiplicationboard"><img src="images/advantage_hdr5.gif"  alt="Division Multiplication" class="hdrimg"/></a>
			<div class="text"><?php include("text/mm_advantage4.txt"); ?></div>	

			<a name="divisionboard"><img src="images/advantage_hdr6.gif"  alt="Division Problems" class="hdrimg"/></a>
			<div class="text"><?php include("text/mm_advantage5.txt"); ?></div>	
			<form name="_xclick" target="paypal" action="https://www.paypal.com" method="post">
			<input name="cmd" value="_cart" type="hidden">
			<input name="business" value="marlmont@aol.com" type="hidden">
			<input name="currency_code" value="USD" type="hidden">
			<input name="item_name" value="Multiplication Division Board Set - Montessori Advantage" type="hidden">
			<input name="item_number" value="MULT_DIV_SET" type="hidden">
			<input name="amount" value="56.00" type="hidden">
			<input name="shipping" value="15.95" type="hidden">
			<input name="shipping2" value="15.95" type="hidden">
			<input src="images/advantage_btn2.gif" name="submit" alt="Multiplication and Division" border="0" type="image">
			<input name="add" value="1" type="hidden">
			</form>


			</td>
		</tr>
	</table></div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
