<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Nature Study at Marlboro Montessori Summer Camp near Freehold, NJ</title>
<meta name="description" content="Our Nature watch program at marlboro montessori summer camp near Freehold, NJ provides the perfect setting for exploration and relaxation."/>
<meta name="keywords" content="Nature Study at Summer Camp NJ, Nature Discovery program NJ, Freehold summer camp nj, Freehold Montessori, Summer Nature Camp, summer camp marlboro New Jersey, Monmouth Couny Summer Nature Camp, Summer Camp Marlboro New Jersey, Morganville Summer Camp nj"/>
</head>
<body>
<div align="center" itemscope itemtype="http://schema.org/Preschool">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent">
	<img src="images/nature_topimg.jpg" alt="Nature Study at Marlboro Montessori" />
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable">
		<tr valign="top">
			<td width="452">
							<img src="images/nature_hdr2.gif" alt="Marlboro Montessori summer Camp and Nature watch"  class="hdrimg"/>
							<div class="text"><?php include("text/mmc_nature2.txt"); ?></div>	
			<img src="images/nature_img1_lft.jpg"  class="sectionImgs" alt="Fun and Educational Natuture study" />
			</td>
			<td width="416">
			<img src="images/nature_img1_rt.jpg"  class="sectionImgs" alt="Table top Cooking" />
				<div class="text"><?php include("text/mmc_nature3.txt"); ?></div>		
			<div class="withSectionBorder">
				<center><img src="images/nature_hdr3.gif"  class="hdrimg" style="margin:5px;" alt="Montessori Summer Camp"/></center>
				<div class="text"><?php include("text/mmc_nature4.txt"); ?></div>		
			</div>

			</td>
		</tr>
		<tr valign="top">
			<td colspan="2">
				<table cellpadding="0" cellspacing="0" align="center" class="contenttable">
					<tr valign="top">
						<td width="586" colspan="2">			
			<img src="images/nature_hdr.gif"  class="hdrimg" alt="Explore and Discover nature">
			<div class="text"><?php include("text/mmc_nature.txt"); ?></div>	
						</td>
						<td width="282" rowspan="2" valign="bottom"><img src="images/nature_img2_rt.jpg"  class="sectionImgs" alt="Herb garden education"/></td>
					</tr>
					<tr valign="top">
						<td valign="bottom"><img src="images/nature_img2_lft.jpg"  class="sectionImgs" alt="Herb garden"/></td>
						<td valign="bottom"><img src="images/nature_img1_ctr.jpg"  class="sectionImgs" alt="Cooking"/></td>
					</tr>
				</table>
			</td>
		</tr>
	</table></div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
