<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Preschool Daycare-Marlboro Montessori Academy-Monmouth County, NJ</title>
<meta name="description" content="">
<meta name="keywords" content="">
</head>
<body>
<div align="center">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent">
	<img src="images/pretoddler_topimg.jpg"  />
			<br/>
			<!--img src="images/pretoddler_hdr.gif"  class="hdrimg"/-->
			<div class="text" style="padding:20px; text-align:justify;">
				<h1 style="color:#B3242C; line-height:28px;">Preschool at Marlboro Montessori Academy</h1>
				<p>
					Our preschool day care program encourages your child's innate ability to absorb information from his or her surroundings. Children who have been educated using the Montessori method grow into competent learners who know how to learn and love learning. The solid foundation begun early in life creates self-confident, contributing adults. The Montessori approach embraces the development of the whole individual, addressing his or her physical, social, emotional, cognitive, and spiritual aspects. 
				</p>
				<h2 style="color:#B3242C; line-height:28px; font-size:23px;">Montessori Child Daycare Provides Social, Emotional and Spiritual Development</h2>
				<img src="images/pretoddler_img3_rt.jpg"  class="sectionImgs" style="float:right; margin-left:10px;"/>
				<p>
					In a Montessori prepared day care school, children are mixed in ages, typically a 3-year developmental span. Mixing ages enables the older, more socially advanced and capable children to become role models for the younger children. A feeling of community develops as younger children are aided by older ones as older children learn patience, nurturance, and an appreciation for other perspectives through their experiences with younger children. In a mixed-age classroom, children can choose friendships based on common interests, not just age.
				</p>
				<p>
					Respect for themselves, for others, and for the environment, forms the basis for all classroom rules. As children are treated with respect by the adults, they learn to treat themselves and others in kind.
				</p>
				<h2 style="color:#B3242C; line-height:28px; clear:both; font-size:23px;">Early Child Care Provides Physical and Motor Development</h2>
				<img src="images/pretoddler_img1_rt.jpg"  class="sectionImgs" style="float:right; margin-left:10px;"/>
				<p>
					To become independent, children must develop motor coordination and control. The freedom of movement found in the Montessori early child care environment allows children the opportunity to learn to control their bodies in a defined space.
				</p>
				<p>
					The activities of Practical Life instill care: for oneself, for others, and for the environment. These exercises include pouring liquids, preparing food, washing dishes, setting a table, tying shoes and zipping jackets, and dealing gracefully and courteously with social encounters. Through these tasks and experiences, children learn to concentrate, coordinate their movements, and develop fine- and large-motor skills. Practical Life activities are the foundation of all future academic work because they promote concentration, order, and a complete work cycle.
				</p>
				<p>
					The sensorial materials are designed to enable young children to identify and refine information obtained through their senses, and to order and classify sensorial impressions. By seeing, smelling, tasting, listening to, touching and further exploring the sensorial properties of these materials, children begin to classify and eventually name objects and attributes in their environment.
				</p>
				<h2 style="color:#B3242C; line-height:28px; clear:both; font-size:23px;">The Best Preschool in Marlboro, New Jersey Provides Cognitive Development</h2>
				<img src="images/pretoddler_img2_rt.jpg"  class="sectionImgs" style="float:right; margin-left:10px;"/>
				<p>
					Because the young preschool child's mind is in such a rapid phase of development, this is the ideal time to assist the development of neural pathways in his/her brain. Montessori observed that young children are in a "sensitive period" for absorbing language, both spoken and written. The Montessori early childhood classroom is rich in oral language opportunities - listening to stories or reciting poems, singing and conversing with others. Introduction of the Montessori sandpaper letters connects each spoken sound with its symbol, supporting the development of writing and, eventually, reading. Young children in preschool are intrigued by numbers - knowing how much or how many provides another dimension in understanding the world. The Montessori math materials and lessons help children to develop an understanding of math concepts through the manipulation of hands-on materials, building a secure foundation of math principles, skills and problem-solving abilities.
				</p>
				<img src="images/pretoddler_img2_lft.jpg"  class="sectionImgs" style="float:right; margin-left:10px;"/>
				<p>
					Science, geography, history, art, and music are all integrated into the early childhood  preschool and daycare environment. They are presented in sensorial ways with specially designed materials and real-life experiences. In geography, children learn not only about the names of countries, but the lives of people and their respective cultures. Preschoolers develop a sense of respect for differences, recognizing that we all belong to the family of people. Young children are natural scientists. Watching and caring for classroom animals and plants creates an interest in science lessons and a reverence for life. Art and music give the children an opportunity for creative and joyful self-expression, as well as experiences with great music and works of art. 
				</p>
				
				<p>
					Each early childhood Montessori material is a concrete representation of a single concept, such as texture, length, or amount. For example, the "pink tower" is made up of ten graduated cubes that vary only by size - not color, pattern, or any other extraneous detail that could distract the child from focusing on stacking the cubes from largest to smallest. And, the "self-correcting" design of the materials allows for any error made in their use to be obvious to the child without intervention of a teacher. Errors are viewed as a necessary and helpful part of the learning process. The Montessori materials provide a bridge from the known to the unknown, allowing children to evolve gradually from concrete, experience-based learning toward increasing abstract thought.
				</p>
				<br style="clear:both;" />
			</div>	
			

				<img src="images/pretoddler_img1_lft.jpg"  class="sectionImgs" />
				
				<br style="clear:both;" />
			
			
			
			
		</div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
