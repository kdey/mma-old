<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Sandpaper Numbers | Marlboro Montessori Academy | New Jersey (NJ)</title>
<meta name="description" content="Durable and washable tactile sandpaper numbers aid in number recognition and formal writing of numbers. "/>
<meta name="keywords" content="Day Care, Montessori teaching aid Sandpaper Numbers, School, Summer Camp, Preschool, Montessori schools in Marlboro, Montessori school in Wickatunk, private schools in Marlboro, N.J.,private schools in Monmouth county, Montessori Learning Center, Montessori learning materials, Montessori teaching materials, Montessori Advantage Learning Center, elementary schools in New Jersey,  Montessori elementary schools in Monmouth county NJ,  IQ, intelligent children, gifted programs for children in NJ, gifted students, tutoring and enrichment, Montessori Tutor, Montessori afterschool tutoring,  Montessori afterschool learning clubs, tutoring for elementary schools"/>
</head>
<body>
<div align="center" itemscope itemtype="http://schema.org/Preschool">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent" style="background-color:#caebfa">
<meta content="Marlboro Montessori Academy" itemprop="name"/>
	<meta content="Durable and washable tactile sandpaper numbers aid in number recognition and formal writing of numbers."/>
	<img src="images/advantage_topimg.jpg" alt="Montessori Math Games" />
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable" itemscope itemtype="http://schema.org/Product">
		<tr valign="top">
			<td width="448">
			<a name="sandpapernumbers"><img src="images/advantage_hdr4.gif"  class="hdrimg" alt="Sandpaper Numbers"/></a>
			<div class="text"><?php include("text/mm_advantage3.txt"); ?></div>	
			<form name="_xclick" target="paypal" action="https://www.paypal.com" method="post">
			<input name="cmd" value="_cart" type="hidden">
			<input name="business" value="marlmont@aol.com" type="hidden">
			<input name="currency_code" value="USD" type="hidden">
			<input name="item_name" value="Sandpaper Numbers - Montessori Advantage" type="hidden">
			<input name="item_number" value="SAND" type="hidden">
			<input name="amount" value="20" type="hidden">
			<input name="shipping" value="6.95" type="hidden">
			<input src="images/advantage_btn2.gif" name="submit" alt="Purchase Item..." border="0" type="image">
			<input name="add" value="1" type="hidden">
			</form>


			</td>
			<td width="399">
			<img src="images/advantage_img1_lft.jpg"  class="sectionImgs" alt="Montessori Learning materials" />



			</td>
		</tr>
	</table></div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
