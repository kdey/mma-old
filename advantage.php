<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Montessori Advantage | Marlboro Montessori, Wickatunk, New Jersey</title>
<meta name="description" content="Marlboro Montessori located in Wickatunk near Matawan provides Educational Manipulative math learning boards for you to use at home with your children."/>
<meta name="keywords" content="Day Care, School, Summer Camp, Preschool, Montessori schools in Marlboro, Montessori school in Wickatunk, private schools in Marlboro, N.J.,private schools in Monmouth county, Montessori Learning Center, Montessori learning materials, Montessori teaching materials, Montessori Advantage Learning Center, elementary schools in New Jersey,  Montessori elementary schools in Monmouth county NJ,  IQ, intelligent children"/>
</head>
<body>
<div align="center" itemscope itemtype="http://schema.org/Preschool">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent" style="background-color:#caebfa">
	<img src="images/advantage_topimg.jpg" alt="Children's Montessori NJ" />
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable">
		<tr valign="top">
			<td width="448">
			<img src="images/advantage_hdr1.gif"  alt="Montessori NJ" class="hdrimg"/>
			<div class="text"><?php include("text/mm_advantage1.txt"); ?></div>	
				</td>
			<td width="399">
			<img src="images/advantage_img1_rt.jpg"  alt="Montessori schools in NJ" class="sectionImgs" />
			</td>
		</tr>
	</table></div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
