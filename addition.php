<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Addition | Subtraction | Montessori Advantage | New Jersey (NJ))</title>
<meta name="description" content="Addition and Subtraction Boards to understand abstract concepts"/>
<meta name="keywords" content="Day Care, School, Summer Camp, Preschool, Montessori schools in Marlboro, Montessori school in Wickatunk, private schools in Marlboro, N.J.,private schools in Monmouth county, Montessori Learning Center, Montessori learning materials, Montessori teaching materials, Montessori Advantage Learning Center, elementary schools in New Jersey,  Montessori elementary schools in Monmouth county NJ,  IQ, intelligent children, gifted programs for children in NJ, gifted students, tutoring and enrichment, Montessori Tutor, Montessori afterschool tutoring,  Montessori afterschool learning clubs, tutoring for elementary schools"/>
</head>
<body>
<div align="center" itemscope itemtype="http://schema.org/Preschool">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent" style="background-color:#caebfa">
<meta content="Marlboro Montessori Academy" itemprop="name"/>
	<meta content="Addition and Subtraction Boards to understand abstract concepts" itemprop="description"/>
	<img src="images/advantage_topimg.jpg" alt="Kids Math"  />
	<span itemscope itemtype="http://schema.org/Product">
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable">
		<tr valign="top">
			<td width="448">
			<img src="images/advantage_hdr1.gif"  alt="Montessori Math Games" class="hdrimg"/>
<br>	
			<a name="additionboard"><img src="images/advantage_hdr2.gif"  alt="Addition Math Material" class="hdrimg"/></a>
			<div class="text"><?php include("text/mm_advantage2.txt"); ?></div>	

			<a name="subtractionboard"><img src="images/advantage_hdr3.gif"  alt="Subtraction Math Activities" class="hdrimg"/></a>
			<div class="text"><?php include("text/mm_advantage7.txt"); ?></div>	
			<form name="_xclick" target="paypal" action="https://www.paypal.com" method="post">
			<input name="cmd" value="_cart" type="hidden">
			<input name="business" value="marlmont@aol.com" type="hidden">
			<input name="currency_code" value="USD" type="hidden">
			<input name="item_name" value="Addition Subtraction Board Set - Montessori Advantage" type="hidden">
			<input name="item_number" value="ADD_SUB_SET" type="hidden">
			<input name="amount" value="56.00" type="hidden">
			<input name="shipping" value="15.95" type="hidden">
			<input name="shipping2" value="15.95" type="hidden">
			<input src="images/advantage_btn1.gif" name="submit" alt="Purchase Item..." border="0" type="image">
			<input name="add" value="1" type="hidden">
			</form>

			</td>
			<td width="399">

			<img src="images/advantage_img2_rt.jpg"  alt="Montessori Addition Subtration" class="sectionImgs" />



			</td>
		</tr>
	</table></span></div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
