<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Marlboro Montessori Leadership Academy | Summer camp near Old Bridge</title>
<meta name="description" content="Marlboro Montessori school in Morganville, New Jersey near Old bridge offers summer camp for developing leadership skills and expand knowledge of your children."/>
<meta name="keywords" content="leadership camp, Manalapan Montessori, leadership summer camp, leadership skills, Summer Camp, Day Care, Old bridge Summer camp, Old Bridge Montessori, Old Bridge Private school, Preschool, After Care, New Jersey, Central New Jersey, Monmouth County"/>
</head>
<body>
<div align="center" itemscope itemtype="http://schema.org/Preschool">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent">
	<img src="images/leadercamp_topimg.jpg" alt="schools private NJ"  />
	
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable">
		<tr valign="top">
			<td width="450">
			<img src="images/leadercamp_hdr.gif"  class="hdrimg" alt="sports camp"/>
			<div class="text"><?php include("text/mm_campleader.txt"); ?></div>	
			<img src="images/leadercamp_img1_lft.jpg"  class="sectionImgs" alt="kindergarten schools leadership camp" />

			</td>
			<td width="417">
			<img src="images/leadercamp_img1_rt.jpg"  class="sectionImgs" alt="shools private camp Matawan"/>
			<div class="text"><?php include("text/mm_campleader2.txt"); ?></div>	
			</td>
		</tr>
	</table></div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>