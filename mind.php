<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Mind Travel at Marlboro Montessori Camp</title>
<meta name="description" content="Mind Travel at Marlboro Montessori Camp"/>
</head>
<body>
<div align="center">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent">
<h1 style="display:none;">Mind Travel at Marlboro Montessori Camp</h1>
	<img src="images/mind_topimg.jpg"  alt="MMA Learning Center in Morganville, NJ"/>
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable">
		<tr valign="top">
			<td width="450">
			<img src="images/mind_hdr.gif"  class="hdrimg"  alt="MMA Learning Center in Morganville, NJ"/>
			<div class="text"><?php include("text/mmc_mind.txt"); ?></div>	
			<img src="images/mind_img1_lft.jpg"  class="sectionImgs"  alt="Mind Travel at Marlboro Montessori Camp"/>
			</td>
			<td width="416">
			<img src="images/mind_img1_rt.jpg"  class="sectionImgs"  alt="MMA Learning Center in Morganville, NJ"/>
			<div class="withSectionBorder">
				<center><img src="images/mind_hdr2.gif"  alt="Mind Travel at Marlboro Montessori Camp"  class="hdrimg" style="margin:5px;"/></center>
				<div class="text" style="margin-left:25px;"><?php include("text/mmc_mind2.txt"); ?></div>		
			</div>
			<img src="images/mind_img2_rt.jpg"  alt="MMA Learning Center in Morganville, NJ" class="sectionImgs" />
			</td>
		</tr>
	</table></div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
