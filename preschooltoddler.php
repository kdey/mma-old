<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Preschool Toddlers | Marlboro Montessori Academy, New Jersey(NJ)</title>
<meta name="description" content="Our school, the Marlboro Montessori Academy has remained centered and steadfast in its mission as a Montessori School."/>
<meta name="keywords" content="Marlboro Montessori Academy, New Jersey Montessori Academy, child care programs, toddler preschool, toddler enrichment programs, montessori toddler curriculum, toddler learning programs, preschool curriculum, preschools, preschool programs, preschool education, preschool learning games, preschool math games, private preschool, preschools in Morganville, montessori preschool curriculum, preschool summer camp, preschool in NJ, NJ Montessori school, montessori school morganville nj, Day care school in New Jersey, Day Care school in nj, Montessori Academy in Monmouth County, Montessori Learning Center, Montessori learning materials, Montessori teaching materials, Montessori school in Wickatunk,private schools in Marlboro, Day Care, private schools in Monmouth county, Montessori child care, Montessori day care, Summer Camp, Day Camp, Ney Jersey, Monmouth County"/>
</head>
<body>
<div align="center" itemscope itemtype="http://schema.org/Preschool">
<meta content="Marlboro Montessori Academy" itemprop="name" />
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent">
	<img src="images/pretoddler_topimg.jpg" alt="Preschool Todller at Marlboro montessori"  />
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable">
		<tr valign="top">
			<td width="452">
			<img src="images/pretoddler_hdr.gif"  class="hdrimg" alt="Toddler Learning program"/>
			<div class="text"><?php include("text/mm_preschooltoddler.txt"); ?></div>	
			<img src="images/pretoddler_img1_lft.jpg"  class="sectionImgs" alt="Learn to build trust" />
			<img src="images/pretoddler_img2_lft.jpg"  class="sectionImgs" alt="Fun Visits" />

			</td>
			<td width="418">
			<img src="images/pretoddler_img1_rt.jpg"  class="sectionImgs" alt="Mothers Day Planning" />
			<img src="images/pretoddler_img2_rt.jpg"  class="sectionImgs" alt="Story Time"/>
			<img src="images/pretoddler_img3_rt.jpg"  class="sectionImgs"  alt="Learn to make friendship"/>

			</td>
		</tr>
	</table></div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
