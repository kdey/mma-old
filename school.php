<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Marlboro Montessori, elementary school at Morganville, New Jersey</title>
<meta name="description" content="The Marlboro Montessori Academy, one of the country's most famous Elementary Schools. Our school is located in Morganville, NJ neighboring Matawan and Manalapan."/>
<meta name="keywords" content="Marlboro Montessori Academy, elementary schools near marlboro, elementary schools Monmouth,  Montessori Academy, Preschool, Manalapan Montessori, Matawan Montessori, Matawan Montessori Day Care, Day care school in New Jersey, Day Care school in nj, Montessori Academy in Monmouth County,Montessori Learning Center, Montessori learning materials, Montessori teaching materials, ontessori school in Wickatunk,private schools in Marlboro, Day Care, private schools in Monmouth county, Montessori child care, Montessori day care, Summer Camp, Day Camp, Ney Jersey, Monmouth County"/>
</head>
<body>
<div align="center" itemscope itemtype="http://schema.org/Preschool">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent">
	<img src="images/ourschool_topimg.jpg" alt="Marlboro Montessori Academy"  />
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable" height="700">
		<tr valign="top">
			<td width="847">
			<img src="images/ourschool_hdr.gif"  class="hdrimg" alt="Unique private school at Marlboro with easy accesibility from Matawan, Manalapan and Freehold"/>
			<div class="text" align="center"><?php include("text/mm_ourschool.txt"); ?></div>	

			</td>
			</tr>
			<tr>
			<td>
			<img src="images/alumni.gif"  class="hdrimg" alt="Alumni Marlboro Montessori Academy Morganville, NJ"/>
			</td>
			<tr>
			<tr>
			<td class="text" align="center">
			<center><img src="images/name.gif" alt="Montessori Alumni"  /></center><br/><br/>
			On the Barbara Walters ABC-TV Special "The 10 Most Fascinating People Of 2004" <span itemprop="alumni">Larry Page and Sergey Brin, founders of the popular Internet search engine Google.com</span>, credited their years as Montessori students as a major factor behind their success. When Barbara Walters asked if the fact that their parents were college professors was a factor behind their success, they said no, that it was their going to a Montessori school where they learned to be self-directed and self-starters. They said that Montessori education allowed them to learn to think for themselves and gave them freedom to pursue their own interests. It is quite an interesting collection of people throughout history who have gone to Montessori schools, sent their children to Montessori schools, or supported this method of education in one way or another. The short list includes:<span itemprop="alumni"> Alice Waters, Friedrich Hundertwasser, Julia Child, Gabriel Garcia Marquez, Helen Keller, Alexander Graham Bell, Thomas Edison, Henry Ford, Mahatma Gandhi, Sigmund Freud, Buckminster Fuller, Leo Tolstoy, Bertrand Russell, Jean Piaget, Erik Erikson, John Holt, Ann Frank, the Dalai Lama, Jacqueline Kennedy, Prince William and Prince Harry of the English royal family, Cher Bono, Yul Brynner, Bill and Hillary Clinton, and Yo Yo Ma.</span><br/><br/>
			</td>
			</tr>
			
			
			<tr>
			<td align="center">
			<iframe width="600" height="300" src="http://www.youtube.com/embed/0C_DQxpX-Kw" frameborder="0" allowfullscreen></iframe><br/><br/>
			</td>
			<!--
			<td width="399">
			<img src="images/ourschool_hdr2.gif"  class="hdrimg"/><br>
			<img src="images/ourschool_img1.jpg"  class="sectionImgs" />

			</td>
			-->
		</tr>
	</table></div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
