<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Marlboro Montessori Academy</title>
</head>
<body style="font-weight:bold; font-family:'Arial';">
<div align="center">
<?php include("inc/header.inc"); ?>
<div class="content hometext" style="margin-top:50px; color:#ffffff;">
<h2>Marlboro Montessori Academy and Day Care April Newsletter</h2>
</div>
<div class="wrapper"><div class="content">
	<div>
		<img src="images/summer-camp-swim-lessons.jpg" alt="Summer Camp Swim Lessons" style="float:left;" /><img src="images/summer-camp-playground.jpg" alt="Summer Day Camp Playground" style="float:right;" />
		<br style="clear:both;" />
	</div>
	<div class="hometext" >
		
		<p>Dear Parents,</p>

<p>We hope everyone has had an enjoyable and restful vacation.  At this time, at MMA, we are all gearing up for summer camp with lots of exciting activities for summer fun! If you visit <a href="http://www.marlborocountrydaycamp.com" title="Marlboro Montessori Summer Day Camp" style="color:#F9F0CF;">Marlboro Montessori Summer Day Camp</a>, you will see information that will soon be sent in a brochure.</p>

<p>Due to an unusual volume of calls for September 2012 enrollment, we will now be designating (732) 946-8887 for families of new prospective students. We ask that you call (732) 946-2267 if you need to speak with Carmella or a staff member.</p>

<p>Nearly two of our Primary classes are completely booked to capacity and Kindergarten is well on its way. Johnny Stenson, from  <a href="http://www.stensondevelopmentsolutions.com" title="Search Engine Optimization/Marketing and Web Design"  style="color:#F9F0CF;">Stenson Development Solutions (Search Engine Optimization/Marketing and Web Design)</a>, has done a fabulous job of creating a stronger presence for MMA on the Internet.</p>

<p>We thank you for all the good news you share with family and friends about our school.  If you want to send a testimonial, the following websites are available to send your comments: <br /> &nbsp;<a href="http://www.greatschools.org" title="Great Schools Reviews"  style="color:#F9F0CF;">www.greatschools.org</a><br />&nbsp;<a href="http://www.education.com" title="Education Reviews"  style="color:#F9F0CF;">www.education.com</a>  </p>  

<p>We have invited a farmer from Quiver Farms to bring 12 eggs, an incubator and a brood box to Montessori on Thursday, April 12th.  Within one week, each egg should start to hatch and adorable little chicks will become a "miracle of life."</p>

<p>Students will share the wonderful experience of birth and how "Mother Nature" provides for creations.</p>

<p>Hola and welcome to the continent of South America!  Get ready as we set out for an adventurous experience as we "walk" through the world's largest jungle and "swim" in the world's second largest river.  Rainforest discussions with topics concerning environmental protection and care of the Earth's resources coincide with Earth Day on April 22nd, and Arbor Day on April 27th.  Throughout this month, MMA students will learn the valuable lessons of "reuse, reduce and recycle".  We hope to instill in every child the many ways to help keep this earth a beautiful place to live!</p>

<p>Graduation letters have been sent home with children entering Kindergarten this coming fall.  Graduation caps and gowns were ordered for participating students and should arrive in May.  Lifetouch Portraits will be here on Monday, April 16th, to make our graduates smile!</p>

<p>Enjoy the fresh air of spring!<br />
Sincerely,<br />
Jean C. Avery</p>
	</div>
	<div id="home4ImgBlock">
		<a href="method.php"><img src="images/home_img1.jpg" border="0" alt="Montessori Curriculum"></a>
		<a href="learningcenter.php"><img src="images/home_img2.jpg" border="0" alt="Montessori Preschool"></a>
		<a href="advantage.php"><img src="images/home_img3.jpg" border="0" alt="Montessori Materials"></a>
		<a href="camp.php"><img src="images/home_img4.jpg" border="0" alt="Summer Camp"></a>
	</div>			
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>