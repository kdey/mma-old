<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Montessori Method at Marlboro Montessori Academy near Matawan, NJ</title>
<meta name="description" content="Marlboro Montessori near Matawan, NJ provides Montessori Method of learning that helps each individual child to reach their full potential in all areas of life."/>
<meta name="keywords" content="Marlboro Montessori Academy, Montessori Method, Manalapan Montessori, Matawan Montessori, Matawan Montessori Learning Materials, Montessori Learning Method, Preschool, Day care school in New Jersey, Day Care school in nj, Montessori Academy in Monmouth County,Montessori Learning Center, Montessori learning materials, Montessori teaching materials, montessori school in Wickatunk,private schools in Marlboro, Day Care, private schools in Monmouth county, Montessori child care, Montessori day care, Summer Camp, Day Camp, Ney Jersey, Monmouth County"/>
</head>
<body>
<div align="center">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent">
	<img src="images/method_topimg.jpg" alt="Montessori Method of Learning"  />
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable" STYLE="margin-left:40px;">
		<tr valign="top">
			<td width="410">
			<img src="images/method_hdr1.gif"  class="hdrimg" alt="Montessori Learning Method at Marlboro Montessori with easy acessibility from Matawan, Manalapan, Freehold"/>
			<div class="text"><?php include("text/mm_method1.txt"); ?></div>	
			</td>
			<td width="420">
			<img src="images/method_img1.jpg"  class="sectionImgs" alt="Child Education at Montessori" />
			</td>
		</tr>
		</table>
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable">
		<tr valign="top">
			<td colspan=2>
			<img src="images/method_hdr2.gif"  class="hdrimg" alt="Montessori Education and Affiliations"/>
			</td>
		</tr>
		</table>
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable" STYLE="margin-left:40px;">
		<tr valign="top">
			<td >
			<img src="images/method_hdr3.gif"  class="hdrimg" alt="Montessori students"/>
			<div class="text"><?php include("text/mm_method2.txt"); ?></div>	
			</td>
			<td>
			<img src="images/method_hdr5.gif"  class="hdrimg" alt="Montessori School"/>
			<div class="text"><?php include("text/mm_method4.txt"); ?></div>	
			</td>
		</tr>
		</table>
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable" STYLE="margin-left:40px;">
		<tr valign="top">
			<td colspan=2>
			<img src="images/method_hdr4.gif"  class="hdrimg" alt="Montessori Connections"/>
			<div class="text"><?php include("text/mm_method3.txt"); ?></div>	
			</td>
		</tr>
	</table></div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>>
