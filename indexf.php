<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Marlboro Montessori Academy | PreSchool, Day Care And Summer Camp | Monmouth County, New Jersey</title>
<META name="description" content="Celebrating 25 years of excellence in Preschool, Summer Camp and Day Care, Marlboro Montessori Academy, Monmouth County, New Jersey (NJ), is committed to providing your child with a safe learning environment based on Montessori Method" />

<META name="keywords" content="Day Care, School, Summer Camp, Preschool, Montessori schools in Marlboro, Montessori school in Wickatunk, private schools in Marlboro, N.J.,private schools in Monmouth county, Montessori Learning Center, Montessori learning materials, Montessori teaching materials, Montessori Advantage Learning Center, elementary schools in New Jersey,  Montessori elementary schools in Monmouth county NJ,  IQ, intelligent children, gifted programs for children in NJ, gifted students, tutoring and enrichment, Montessori Tutor, Montessori afterschool tutoring,  Montessori afterschool learning clubs,  tutoring for elementary schools, swim instructions, nature study, green hour for children,  Biome study, children's ampitheater, day camps in Marlboro, Monmouth county day care,  Montessori school extended hours, Montessori child care, Montessori day care, NJ private schools, Great schools, extended hours at summer camp, Nature study at Montessori, birdwatching for children, Math learning boards, teaching camp, Tutoring help with no child left behind, best schools and camps." />
<link rel="stylesheet" href="mmm.css" type="text/css">

<style type="text/css">
<!--
.style1 {
	font-size: 14px
}
.style2 {
	color: #FFFF33;
	font-style: italic;
}
.style8 {font-size: 24px}
a:link {
	color: #FF6;
}
a:visited {
	color: #FF6;
}
a:hover {
	color: #FF6;
}
a:active {
	color: #FF6;
}

</style>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>




<!--................index ani........................-->
<script type="text/javascript" src="js/jqery.js"></script>
<script type="text/javascript" src="js/script1.js"></script>

<body>
<div align="center">
  <?php include("inc/header.inc"); ?>
  <div class="wrapper" itemscope itemtype="http://schema.org/Preschool">
    <div class="content">
      <form name="form1" method="post" action="">
      </form>
    <br>
    <div id="home4ImgBlock">
      <table width="907" border="0" height="1250" cellspacing="0" cellpadding="0">
     	<tr valign="top">
			<td width="686">
				<table width="686" border="0">
				
					<tr>
						<td width="686" align="left" valign="top">
			  	<div id="header_ani">
	<!-- jQuery handles to place the header background images -->
	<div id="headerimgs">
		<div id="headerimg1" class="headerimg"></div>
		<div id="headerimg2" class="headerimg"></div>
	</div>

	<div id="headernav-outer">
		<div id="headernav">
			<div id="back" class="btn"></div>
			<div id="control" class="btn"></div>
			<div id="next" class="btn"></div>
		</div>
	</div>

</div>
			  
			  </td>
					</tr>
					
					<tr>
						<td width="685" border="0">
							<div class="hometext_top">
				<h1 style="text-align:center; line-height:30px;">Welcome to <span itemprop="name"> Marlboro Montessori Academy</span>:<br /> School, Summer Camp &amp; Day Care</h1>
			</div>
						
						</td>
					</tr>
					
					<tr>
						<td width="685" class="hometext_bottom" border="0">
							 
		
			
				<?php include("text/mm_home.txt"); ?><br/><br/>
				Featuring<span itemprop="rating"> 5 star ratings</span> in <a href="http://www.greatschools.org/new-jersey/wickatunk/3065-Marlboro-Montessori-Academy/" style="color:#FFFFFF; text-decoration:underline; font-family:sans-serif,Arial, Helvetica; font-size:14px;" target="new">GreatSchools.org</a>, Marlboro Montessori Academy is one of the most renowned private schools in the area.
				<br/><br/>
				<span itemprop="makesoffer" itemscope itemtype="http://schema.org/MakesOffer">
				Our sensational <span itemprop="makesoffer">Day Camp and Summer Camp for kids </span> offers a complete range of <span itemprop="makesoffer">summer activities for kids</span>. In addition to an on-site, <span itemprop="makesoffer">in-ground swimming program with trained instructors</span> we are proud to introduce the <span itemprop="makesoffer">Children''s Theatre Workshop</span> this summer. Our activities provide a healthy balance of <span itemprop="makesoffer">education and time in the sun</span>. Children of all ages love to pretend and enter worlds of the imagination. Our <span itemprop="makesoffer">Nature Explore Program</span> offers an amazing<span itemprop="makesoffer"> natural outdoor classroom</span>. The <span itemprop="makesoffer">Kids Learning Caf� provides special time to explore <span itemprop="makesoffer">cooking creativity</span> come alive in our kid''s kitchen. A special section on the East side of the campus is also reserved for <span itemprop="makesoffer">bird observation</span>.<br/><br/></span>
				<span itemprop="contactpoint" itemscope itemtype="http://schema.org/ContactPoint">
				Call us for your summer camp reservation now <span itemprop="telephone">732.946.CAMP</span> or email is at <span itemprop="email"><a href=marlmont@aol.com>marlmont@aol.com</a></span>
				
				</span>
				<p style="text-align:right;"><a title="Marlboro Montessori NJ School Newsletter" href="mma_newsletter.pdf" style="color:#F9F0CF; font-size:15px;" target="new">Mar 2013 School Newsletter</a></p>
			
			
		 
						
						</td>
					</tr>
					
					
				</table>
			
			</td>
			
			<td width="214" align="right" bgcolor="#fdf679" valign="top">
				 <table width="211" border="0" align="left" cellpadding="0"cellspacing="6" bordercolor="#FFFFFF"  bordercolorlight="#FFFFFF" bordercolordark="#FFFF00">
            <tr>
              <td width="199" height="55" align="center" valign="top" bgcolor="#046bb8" class="footer"><table width="204" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="204" align="right" valign="top" bgcolor="#046bb8"><img src="images/summer_camp.jpg" width="200" height="350" alt="Chef James Avery"></td>
                </tr>
				 <tr>
                      <td align="left" valign="top" class="footer">&nbsp;</td>
                    </tr>
				
              </table></td>
            </tr>
            <tr valign="top">
              <td height="22" align="center" bgcolor="#103569" class="footer"><table width="190" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="204" align="center" valign="top" bgcolor="#046bb8"><a href="http://www.montessoriadvantage.com" target="_blank"><img src="images/montessoriadvantage.jpg" width="200" height="164" alt="Montessori Advantage Products"></a></td>
                </tr>
                <tr bgcolor="#046bb8">
                  <td align="left" valign="top"><table width="100%" border="0" cellspacing="6" cellpadding="0">
                    <tr>
                      <td align="left" valign="top" class="footer">The Montessori Research Institute in collaboration with the Marlboro  Montessori Academy have designed and developed manipulative math  learning boards which are now available for you to purchase and use at  home with your children. <a href="http://www.montessoriadvantage.com" title="Montessori Advantage Products" target="_blank">Click here</a> to find out more.<br></td>
                    </tr>
                  </table></td>
                </tr>
              </table></td>
            </tr>
            <tr bgcolor="#046bb8">
              <td height="55" align="center" valign="middle" class="footer"><a title="Marlboro Montessori NJ School Newsletter" href="mma_newsletter.pdf" target="_blank"><strong>Montessori Newsletter</strong></a></td>
            </tr>
            <tr bgcolor="#046bb8">
              <td height="55" align="center" valign="middle" class="footer"><a href="http://marlboromontessori.blogspot.com/" title="Marlboro Montessori Blog"><strong>Montessori Blog</strong></a></td>
            </tr>
          </table>
			
			</td>
		</tr>
		
		<tr><td colspan="2" height="10">&nbsp;</td></tr>
		
		<tr>
			<td width="903" colspan="2">
				<div id="home4ImgBlock">
			<a href="method.php"><img src="images/home_img1.jpg" border="0" alt="Montessori Curriculum"></a>
			<a href="learningcenter.php"><img src="images/home_img2.jpg" border="0" alt="Montessori Preschool"></a>
			<a href="advantage.php"><img src="images/home_img3.jpg" border="0" alt="Montessori Materials"></a>
			<a href="camp.php"><img src="images/home_img4.jpg" border="0" alt="Summer Camp"></a>
		</div>
			
			</td>
		</tr>

      </table>
    </div>			
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>


</body>
</html>
