<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Marlboro Montessori - Day Care & Summer Camp, Monmouth County New Jersey (NJ)</title>
</head>
<body>
<div align="center">
<?php include("inc/header2.inc"); ?>
<div class="wrapper"><div class="content">
	<img src="images/camp_topimg.jpg"  /><br>
	<div class="hometext"><?php include("text/mm_camp2.txt"); ?></div>
	<div id="home5ImgBlock">
		<a href="mind.php"><img src="images/camp_img1.jpg" border="0" alt="Mind Travels"></a>
		<a href="nature.php"><img src="images/camp_img2.jpg" border="0" alt="Nature Watch"></a>
		<a href="swim.php"><img src="images/camp_img3.jpg" border="0" alt="Swim School"></a>
		<a href="leadershipcamp.php"><img src="images/camp_img4.jpg" border="0" alt="Leadership Academy"></a>
		<img src="images/camp_img5.jpg" border="0" alt="Montessori Schools" usemap="#home_img5" alt="Montessori Preschool" style="border-style:none" />
		<map id="home_img5" name="home_img5">
		<area shape="rect" alt="Matawan NJ Montessori" coords="3,152,171,302" href="advantage.php" title="" />
		<area shape="rect" alt="Old Bridge NJ Montessori" coords="2,3,177,141" href="learningcenter.php" title="" />
		<area shape="default" nohref="nohref" alt="Monmouth County Montessori" />
		</map>
	</div>			
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
