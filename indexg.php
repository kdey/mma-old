<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Marlboro Montessori Academy</title>


<style type="text/css">
<!--
.style1 {
	font-size: 14px
}
.style2 {
	color: #FFFF33;
	font-style: italic;
}
.style8 {font-size: 24px}
a:link {
	color: #FF6;
}
a:visited {
	color: #FF6;
}
a:hover {
	color: #FF6;
}
a:active {
	color: #FF6;
}

</style>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>

<link rel="stylesheet" href="mm1.css" type="text/css">


<!--................index ani........................-->
<script type="text/javascript" src="js/jqery.js"></script>
<script type="text/javascript" src="js/script1.js"></script>

<body>
<div align="center">
  <?php include("inc/header.inc"); ?>
  <div class="wrapper">
    <div class="content">
      <form name="form1" method="post" action="">
      </form>
    <br>
    <div id="home4ImgBlock">
      <table width="903" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="685" rowspan="2" align="left" valign="top">
		  
		  <table width="685" border="0" align="left" cellpadding="0" cellspacing="0">
            <tr>
              <td width="686" align="left" valign="top">
			  	<div id="header_ani">
	<!-- jQuery handles to place the header background images -->
	<div id="headerimgs">
		<div id="headerimg1" class="headerimg"></div>
		<div id="headerimg2" class="headerimg"></div>
	</div>

	<div id="headernav-outer">
		<div id="headernav">
			<div id="back" class="btn"></div>
			<div id="control" class="btn"></div>
			<div id="next" class="btn"></div>
		</div>
	</div>

</div>
			  
			  </td>
            </tr>
			
			
            <tr>
              <td height="191" align="left" valign="top" bgcolor="#046bb8"><table width="99%" border="0" cellspacing="10" cellpadding="0">
                <tr>
                  <td height="171" align="left" valign="top" bgcolor="#046bb8"><span class="footer">The Montessori Method is unsurpassed as the most outstanding and valuable   educational contribution to the entire world! It is the only educational   approach that is accompanied by very sophisticated and effective learning   materials that have been tested for over a century. Maintaining an extraordinary   track record, Montessori has moved into the 21st century with great honor and   recognition from educators and families on all seven continents. Founded in   1978, Marlboro Montessori Academy is one of the premier Montessori schools in   central New Jersey. Offering half day, full day, extended care and summer camp   activities, we have students from various towns including Matawan, Freehold, Old   Bridge, Manalapan, Marlboro, Aberdeen, Hazlet, South Amboy, Millstone,   Englishtown, Holmdel, Colts Neck, Howell, Little Silver, Wickatunk, Middletown   and others. </span></td>
                </tr>
              </table></td>
            </tr>
			
			
            <tr>
              <td height="336" align="left" valign="top" bgcolor="#046bb8"><table width="686" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="234" bgcolor="#046bb8"><img src="images/block1.jpg" width="224" height="224" alt="The Most Powerful Educational Techniques"></td>
                  <td width="224" bgcolor="#046bb8"><img src="images/block2.jpg" width="224" height="224" alt="Tutoring and Enrichment using the Montessori Method"></td>
                  <td width="228" bgcolor="#046bb8"><img src="images/block3.jpg" width="224" height="224" alt="Extroardinary Swim Program at Marlboro Montessori Camp"></td>
                </tr>
                <tr>
                  <td height="90" align="center" valign="top" bgcolor="#046bb8"><em class="sechdr"><br>
                      <span class="hometextb">&quot;The Most Powerful Educational Techniques<br>
in the World&quot; </span></em></td>
                  <td align="center" valign="top" bgcolor="#046bb8"><br>                    <span class="hometextb"><em class="hometextb"><strong>&quot;Tutoring &amp; Enrichment<br>
                    using the Montessori<br>
                    Method. Remarkable</strong></em><strong><em class="hometextb"><br>
                    Results
&quot;</em></strong></span></td>
                  <td align="center" valign="top" bgcolor="#046bb8"><em class="sechdr"><br>
                      <span class="hometextb">&quot;Extraordinary Swim Program and High Energy Camp  Activities&quot;</span></em></td>
                </tr>
              </table></td>
            </tr>
			
			
            <tr bgcolor="#046bb8">
              <td height="19" align="left" valign="top">&nbsp;</td>
            </tr>
			
			
          </table>
		  </td>
		  
		  
          <!--<td width="24%" height="499" align="right" valign="top" bgcolor="#fdf679"><table width="211" border="0" align="left" cellpadding="0"cellspacing="6" bordercolor="#FFFFFF"  bordercolorlight="#FFFFFF" bordercolordark="#FFFF00">
            <tr>
              <td width="199" height="55" align="center" valign="middle" bgcolor="#046bb8"><a href="http://networking.marlboromontessoriacademy.com/" title="Click Here to view Montessori Network" target="_blank"><strong class="footer">Marketing Sense<br>
                For Montessori Schools
              </strong></a></td>
            </tr>
            <tr>
              <td height="55" align="center" valign="top" bgcolor="#046bb8" class="footer"><table width="204" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="204" align="right" valign="top" bgcolor="#046bb8"><a href="http://www.chefjamesavery.com" target="_blank"><img src="images/chefjamesavery.jpg" width="200" height="164" alt="Chef James Avery"></a></td>
                </tr>
                <tr bgcolor="#046bb8">
                  <td align="left" valign="top" bgcolor="#046bb8"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="6">
                    <tr>
                      <td align="left" valign="top" class="footer">Sign up for Le Petit  Chef after school program. Chef James Avery is available for hire as a personal  chef or catering large &amp; small parties.<br>
                        <a href="http://www.chefjamesavery.com" title="Click Here to find out more about Chef James Avery" target="_blank"><strong>www.chefjamesavery.com</strong></a></td>
                    </tr>
                  </table></td>
                </tr>
              </table></td>
            </tr>
            <tr valign="top">
              <td height="22" align="center" bgcolor="#103569" class="footer"><table width="190" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="204" align="center" valign="top" bgcolor="#046bb8"><a href="http://www.montessoriadvantage.com" target="_blank"><img src="images/montessoriadvantage.jpg" width="200" height="164" alt="Montessori Advantage Products"></a></td>
                </tr>
                <tr bgcolor="#046bb8">
                  <td align="left" valign="top"><table width="100%" border="0" cellspacing="6" cellpadding="0">
                    <tr>
                      <td align="left" valign="top" class="footer">The Montessori Research Institute in collaboration with the Marlboro  Montessori Academy have designed and developed manipulative math  learning boards which are now available for you to purchase and use at  home with your children. <a href="http://www.montessoriadvantage.com" title="Montessori Advantage Products" target="_blank">Click here</a> to find out more.<br></td>
                    </tr>
                  </table></td>
                </tr>
              </table></td>
            </tr>
            <tr bgcolor="#046bb8">
              <td height="55" align="center" valign="middle" class="footer"><a href="montessorihighlights.php" title="Montessori Highlights"><strong>Montessori Highlights</strong></a></td>
            </tr>
            <tr bgcolor="#046bb8">
              <td height="55" align="center" valign="middle" class="footer"><a href="montessorialumni.php" title="Marlboro Montessori Alumni"><strong>Marlboro Montessori Alumni</strong></a></td>
            </tr>
            <tr bgcolor="#046bb8">
              <td height="55" align="center" valign="middle"><a href="http://pub35.bravenet.com/guestbook/2963314505/" title="Sign Our Guestbook" target="_blank"><span class="footer"><br>
                Sign Our Guestbook</span></span><br> 

                              &nbsp;</a></td>
            </tr>
          </table></td>-->
        </tr>
        <tr>
          <td height="19" align="right" valign="top" bgcolor="#fdf679">&nbsp;</td>
        </tr>
      </table>
    </div>			
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>


</body>
</html>
