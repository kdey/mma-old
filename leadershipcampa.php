<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Leadership Summer Camp at Marlboro Montessori Academy</title>
</head>
<body>
<div align="center">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent"><img src="images/leadercamp_topimga.jpg" width="903" height="186">
  <table cellpadding="0" cellspacing="0" align="center" class="contenttable">
    <tr valign="top">
			<td width="450">
			<img src="images/leadercamp_hdr.gif"  class="hdrimg" alt="sports camp"/>
			<div class="text"><?php include("text/mm_campleader.txt"); ?></div></td>
			<td width="417"><table width="100%" border="0" cellpadding="0">
			  <tr>
			    <td align="right" valign="top"><img src="images/leadershipa.jpg" width="416" height="359"></td>
			    </tr>
			  <tr>
			    <td height="28" class="secsubhdr">Learning in a circle about different parts of the world.</td>
			    </tr>
			  <tr>
			    <td align="right" valign="top"><img src="images/leadershipb.jpg" width="416" height="359"></td>
			    </tr>
			  <tr>
			    <td height="28" class="secsubhdr">Having fun with your friends in another form of water.</td>
			    </tr>
		    </table></td>
		</tr>
	</table></div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>