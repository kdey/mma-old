<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Admissions at Marlboro Montessori | Morganville near Manalapan, NJ</title>
<meta name="description" content="Marlboro Montessori Academy welcomes all prospective applicants and their families. We are located in Morganville with easy access from Matawan and Manalapan."/>
<meta name="keywords" content="Day Care Admission, CHILD'S ADMISSION, School, Summer Camp, Preschool, Montessori schools in Marlboro, Admission in Montessori schools in Marlboro, Montessori school in Wickatunk, private schools in Marlboro, N.J.,private schools in Monmouth county, Montessori Learning Center, Montessori learning materials, Montessori teaching materials, Montessori Advantage Learning Center, elementary schools in New Jersey,  Montessori elementary schools in Monmouth county NJ,  IQ, intelligent children, gifted programs for children in NJ, gifted students, tutoring and enrichment, Montessori Tutor, Montessori afterschool tutoring,  Montessori afterschool learning clubs, tutoring for elementary schools"/>
</head>
<body>
<div align="center" itemscope itemtype="http://schema.org/Preschool">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent">
	<img src="images/admissions_topimg.jpg" alt="Montessori Admissions" />
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable">
		<tr valign="top">
			<td width="481">
			<img src="images/admissions_hdr.gif"  alt="Montessori Schools" class="hdrimg"/>
			<div class="text"><?php include("text/mm_admissions.txt"); ?></div>	
			</td>
			<td width="381">
			<img src="images/admissions_img1.jpg"  alt="Montessori Preschool near Matawan" class="sectionImgs" />

			</td>
		</tr>
	</table></div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
