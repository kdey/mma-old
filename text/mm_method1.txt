<h1 style="color:#333333; margin:0px 25px -20px 0; font-size:18px;text-align:left; line-height:22px; font-weight:300">Maria Montessori developed "beautiful" materials that stimulate, isolate, and self-correct a specific activity or skill.</h1>
<br/><br/>Our Montessori materials are multi-sensory and demonstrate concepts at the concrete level.  Once a child understands at a concrete level they can move on to more abstract concepts.

<br><br>

The goal of a Montessori program at Marboro Montessori conveniently located in <span itemprop="location">Morganville</span> with easy accessibility from <span itemprop="location">Old Bridge</span>, <span itemprop="location">Freehold</span>, <span itemprop="location">Matawan</span>, <span itemprop="location">Manalapan</span> and Morganville, is to help each individual child reach their full potential in all areas of life. All of our activities at MMA promote the development of social skills, emotional growth, physical coordination and cognitive preparation.  We empower children to ignite their natural curiosity!

