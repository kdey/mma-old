<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Swimming at Marlboro Montessori Summer Camp near Matawan, New Jersey</title>
<meta name="description" content="Marlboro Montessori school is conveniently located near Matawan in Morganville, NJ. We offer aquatic program at our swimming school at Wickatunk in this summer."/>
<meta name="keywords" content="Swim camp New Jersey, Swim camp Wickatunk, Manalapan Montessori, Matawan Montessori, Summer Camp Wickatunk, Summer Camp Matawan, Swim camp nj, Day Camp Ney Jersey, Monmouth County summer camp, morganville summer camp nj, summer camp marlboro nj, Swim camp marlboro nj, summer camp marlboro new jersey"/>
</head>
<body>
<div align="center" itemscope itemtype="http://schema.org/Preschool">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent">
	<img src="images/swim_topimga.jpg" alt="Swim School at Marboro Montessori near Manalapan" />
	<table height="789" align="center" cellpadding="0" cellspacing="0" class="contenttable">
		<tr valign="top">
			<td width="450" rowspan="4">
			<img src="images/swim_hdr.gif"  class="hdrimg" alt="Swim Camp at Marlboro Montessori with easy accessibility from Matawan, Manalapan, Freehold"/>
			<div class="text"><?php include("text/mmc_swim.txt"); ?></div>
			<div class="text"><?php include("text/mmc_swim2.txt"); ?>
			  <br>
			  <br>
			  <table width="100%" border="0" cellpadding="0">
			    <tr>
			      <td colspan="2" align="left" valign="top" class="text"><em>Click on boxes below to view our swimming instruction</em></td>
			      </tr>
			    <tr>
			      <td width="50%" height="29" align="center" valign="middle" class="secsubhdr"><a href="MOV02460.MPG" target="_blank"><img src="images/clip.jpg" width="93" height="71" alt="Class Instruction"></a></td>
			      <td width="50%" align="center" valign="middle" class="secsubhdr"><a href="MOV02477.MPG" target="_blank"><img src="images/clip1.jpg" width="92" height="72" alt="Individual Instruction"></a></td>
			      </tr>
			    </table>
            </div>	
			</td>
			<td width="416" height="359" align="right"><img src="images/swim_a.jpg" width="416" height="359" alt="Sumer Camp Hawaiian Loui"></td>
		</tr>
		<tr valign="top">
		  <td height="43" align="center" valign="middle" class="secsubhdr"><h1 style="color:#103569;font-weight:bold;font-size:15px;text-align:center;">At Marlboro Montessori Camp your child</h1>
		    will experience a  Hawaiian Loui?</td>
		  </tr>
		<tr valign="top">
		  <td height="359" align="right"><img src="images/swim_d.jpg" width="416" height="359" alt="Making friends at camp"></td>
		  </tr>
		<tr valign="top">
		  <td height="26" align="center" valign="middle"><span class="secsubhdr">Making friends at camp is what it is all about.</span></td>
		  </tr>
	</table>
</div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
