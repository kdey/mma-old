<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Childrens Theatre at Marlboro Montessori Camp</title>
</head>
<body>
<div align="center">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent">
	<img src="images/hd_bg.jpg"  />
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable">
		<tr valign="top">
			<td width="430" style="padding:0 0 0 20px;">
				<img src="images/children_theatre.png"  class="hdrimg"/><br/>
				<div class="text">New to camp this summer is the Children's Theatre Workshop. Children of all ages love to pretend and enter worlds of the imagination. Our camp will provide your child with a creative and fun journey into the multi-sensory world of live theatre. Campers will participate in a variety of dramatics, improvisations and theater exercises similar to those done in a professional troupe. Miss Jamian, children's theatre teacher as well as music and singing teacher will be with us to foster life skills through stage skills. Self confidence will soar for our campers this summer!</div>	
			</td>
			<td width="450" align="center"><img src="images/img1.jpg"  class="hdrimg"/><br/></td>
			
			<tr>
			
			<td width="450" align="center"><img src="images/img4.jpg"  class="hdrimg"/><br/></td>
			<td width="430" style="padding:0 0 0 20px;" valign="top"><div class="withSectionBorder">
				<img src="images/mind_hdr2.gif"  class="hdrimg" style="margin:5px;"/>
				<div class="text" style="margin-left:30px;"><?php include("text/mmc_mind2.txt"); ?></div>		
			</div></td>
			
			</tr>
			
		</tr>
		
		<!--<tr>
			<td width="903" align="center" colspan="2"><img src="images/photoframe.png"  class="hdrimg"/><br/></td>
			
			
		</tr>-->
	</table>
	
	
	</div><br/>
	<img src="images/photoframe.png"  class="hdrimg"/>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
