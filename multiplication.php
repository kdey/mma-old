<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Multiplication | Division | Montessori Advantage | New Jersey (NJ))</title>
<meta name="description" content="Multiplication and Division Boards to understand abstract concepts"/>
<meta name="keywords" content="Day Care, School, Summer Camp, Preschool, Montessori schools in Marlboro, Montessori school in Wickatunk, private schools in Marlboro, N.J.,private schools in Monmouth county, Montessori Learning Center, Montessori learning materials, Montessori teaching materials, Montessori Advantage Learning Center, elementary schools in New Jersey,  Montessori elementary schools in Monmouth county NJ,  IQ, intelligent children, gifted programs for children in NJ, gifted students, tutoring and enrichment, Montessori Tutor, Montessori afterschool tutoring,  Montessori afterschool learning clubs, tutoring for elementary schools"/>
</head>
<body>
<div align="center">
<?php include("inc/header.inc"); ?>
<div class="wrapper" itemscope itemtype="http://schema.org/Preschool"><div class="content"><div class="maincontent" style="background-color:#caebfa">
	<img src="images/advantage_topimg.jpg" alt="Preschool math games"  />
	<meta content="Marlboro Montessori Academy" itemprop="name"/>
	<meta content="Multiplication and Division Boards to understand abstract concepts" itemprop="description"/>
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable">
		<tr valign="top">
			<td width="448">
			<img src="images/advantage_hdr1.gif"  class="hdrimg" alt="Math manipulatives"/><BR>		
			<a name="multiplicationboard"><img src="images/advantage_hdr5.gif"  class="hdrimg" alt="Multiplication Board"/></a>
			<div class="text"><?php include("text/mm_advantage4.txt"); ?></div>	

			<a name="divisionboard"><img src="images/advantage_hdr6.gif"  class="hdrimg" alt="Division Board"/></a>
			<div class="text"><?php include("text/mm_advantage5.txt"); ?></div>	
			<form name="_xclick" target="paypal" action="https://www.paypal.com" method="post">
			<input name="cmd" value="_cart" type="hidden">
			<input name="business" value="marlmont@aol.com" type="hidden">
			<input name="currency_code" value="USD" type="hidden">
			<input name="item_name" value="Multiplication Division Board Set - Montessori Advantage" type="hidden">
			<input name="item_number" value="MULT_DIV_SET" type="hidden">
			<input name="amount" value="56.00" type="hidden">
			<input name="shipping" value="15.95" type="hidden">
			<input name="shipping2" value="15.95" type="hidden">
			<input src="images/advantage_btn3.gif" name="submit" alt="Purchase Item..." border="0" type="image">
			<input name="add" value="1" type="hidden">
			</form>
				</td>
			<td width="399">
		<img src="images/advantage_img2_lft.jpg"  class="sectionImgs" alt="Montessori math learning materials" />


			</td>
		</tr>
	</table></div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
