<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Marlboro Montessori Academy / Day Care- Monmouth County, NJ</title>
<link rel="stylesheet" href="mm.css" type="text/css">

</head>
<body style="font-weight:bold; font-family:'Arial';">
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/script.js"></script>
		

<div id="container">
 <a href="https://www.facebook.com/MarlboroMontessoriAcademy?ref=hl" title="Join Us On Facebook"><img src="http://www.facebookicons.net/images/76.png" alt="Icon" width="50px" height="50px" /></a>
</div>
<div align="center" itemscope itemtype="http://schema.org/Preschool">
<!--<meta itemprop="name" content="Marlboro Montessori Academy / Day Care- Monmouth County, NJ"/>
<meta itemprop="description" content="Marlboro Montessori Academy celebrates 25 years, using the most powerful educational techniques in the world!  The academy provides toddlers through the fourth grade an opportunity to achieve their potential by nurturing and stimulating the mind, body and spirit." />-->
<?php include("inc/header2.inc"); ?>
<!--div class="content hometext" style="margin-top:50px; color:#ffffff;">
<p class="hometext">In addition to day care, our <a href="learningcentre.php"  style="color:#F9F0CF;" title="Marlboro NJ Montessori Leadership Academy">Leadership Academy</a> for elementary students fosters high self esteem based on the principles of Multiple Intelligence.</p>
</div-->
<div class="wrapper">

	<div class="content">
	 <div class="animation_area">
		<div id="header_ani">
	<!-- jQuery handles to place the header background images -->
	<div id="headerimgs">
		<div id="headerimg1" class="headerimg"></div>
		<div id="headerimg2" class="headerimg"></div>
	</div>

	<div id="headernav-outer">
		<div id="headernav">
			<div id="back" class="btn"></div>
			<div id="control" class="btn"></div>
			<div id="next" class="btn"></div>
		</div>
	</div>

</div>
		
		</div>
	
		<div class="hometext">
			<div class="hometext_top">
				<h1 style="text-align:center; line-height:40px;">Welcome to <span itemprop="name"> Marlboro Montessori Academy</span>:<br /> School, Summer Camp &amp; Day Care</h1>
				<meta itemprop="description" content="Marlboro Montessori Academy celebrates 25 years, using the most powerful educational techniques in the world!  The academy provides toddlers through the fourth grade an opportunity to achieve their potential by nurturing and stimulating the mind, body and spirit." />
			</div>
		
		<div class="hometext_bottom">
		
			<div class="hometext_bottom_left">
				<?php include("text/mm_home.txt"); ?><br/><br/>
				Featuring<span itemprop="rating"> 5 star ratings</span> in <a href="www.GreatSchools.org" style="color:#FFFFFF; text-decoration:underline; font-family:sans-serif,Arial, Helvetica; font-size:14px;">GreatSchools.org</a>, Marlboro Montessori Academy is one of the most renowned private schools in the area.
				<br/><br/>
				<span itemprop="makesoffer" itemscope itemtype="http://schema.org/MakesOffer">
				Yes, Marlboro Montessori Country <span itemprop="makesoffer">Day Camp</span>,<meta itemprop="makesoffer" content="full day, half day and summer camp activities"> located on the prestigious campus of the Marlboro Montessori Academy, is gearing up for a <span itemprop="makesoffer">sensational summer program</span>. In addition to an on-site, <span itemprop="makesoffer">in-ground swimming program</span> the camp is ready to do some very <span itemprop="makesoffer">adventurous and exciting activities</span>.<br/><br/>
				New to camp this summer is the <span itemprop="makesoffer">Children's Theatre Workshop</span>. Children of all ages love to pretend and enter worlds of the imagination. Our <span itemprop="makesoffer">Nature Explore Program</span> will offer an amazing<span itemprop="makesoffer"> natural outdoor classroom</span>. Special days for..." BRNG YOUR BIKE TO CAMP� should be a tremendous success for elementary age campers over 8 years old. The Kids Learning Caf� will provide special time to have all senses come alive in our kid's kitchen. A special section on the East side of the campus will be reserved for <span itemprop="makesoffer">bird observation</span>.<br/><br/></span>
				<span itemprop="contactpoint" itemscope itemtype="http://schema.org/ContactPoint">
				Call us for your summer camp reservation now <span itemprop="telephone">732.946.CAMP</span> or email is at <span itemprop="email">marlmont@aol.com</span>
				</span>
			</div>
			
		  </div>
		<!--<p style="text-align:right;"><a title="Marlboro Montessori NJ School Newsletter" href="april-2012-school-newsletter.php" style="color:#F9F0CF; font-size:26px;">April 2012 School Newsletter</a></p>-->
		
		</div>
		<div id="home4ImgBlock">
			<a href="method.php"><img src="images/home_img1.jpg" border="0" alt="Montessori Curriculum"></a>
			<a href="learningcenter.php"><img src="images/home_img2.jpg" border="0" alt="Montessori Preschool"></a>
			<a href="advantage.php"><img src="images/home_img3.jpg" border="0" alt="Montessori Materials"></a>
			<a href="camp.php"><img src="images/home_img4.jpg" border="0" alt="Summer Camp"></a>
		</div>	
		
		<?php include("inc/footer.inc"); ?>
	</div>

</div>

</div>

        <!-- JavaScript includes -->
<!--<script src="js/jquery.js" type="text/javascript"></script>-->
<script type="text/javascript">
        $(document).ready(function() {

            $(".signin").click(function(e) {          
				e.preventDefault();
                $("fieldset#signin_menu").toggle();
				$(".signin").toggleClass("menu-open");
            });
			
			$("fieldset#signin_menu").mouseup(function() {
				return false
			});
			$(document).mouseup(function(e) {
				if($(e.target).parent("a.signin").length==0) {
					$(".signin").removeClass("menu-open");
					$("fieldset#signin_menu").hide();
				}
			});			
			
        });
</script>
<script src="js/jquery.tipsy.js" type="text/javascript"></script>
<script type='text/javascript'>
    $(function() {
	  $('#forgot_username_link').tipsy({gravity: 'w'});   
    });
  </script>
</body>
</html>
