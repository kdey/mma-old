<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Nature Study at Marlboro Montessori Camp</title>
</head>
<body>
<div align="center">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content"><div class="maincontent"><img src="images/nature_topimga.jpg" width="903" height="185">
  <table cellpadding="0" cellspacing="0" align="center" class="contenttable">
    <tr valign="top">
      <td width="452">
        <img src="images/nature_hdr2.gif"  class="hdrimg"/>
        <div class="text">
          <p>
            <?php include("text/mmc_nature.txt"); ?>
            </p>
          <p>
            <?php include("text/mmc_nature2.txt"); ?>
          </p>
          <p>
            <?php include("text/mmc_nature3.txt"); ?>
          </p>
        </div></td>
      <td width="416"><table width="100%" border="0" cellpadding="0">
        <tr>
          <td align="right" valign="top"><img src="images/natureb.jpg" width="416" height="359"></td>
        </tr>
        <tr>
          <td height="44" class="secsubhdr">At MMCamp the children also study the Sea with it's many creatures within.</td>
        </tr>
        <tr>
          <td align="right" valign="top"><img src="images/nature1.jpg" width="416" height="359"></td>
        </tr>
        <tr>
          <td height="26" class="secsubhdr">Dress-up time to let the kid's just be kids.</td>
        </tr>
      </table></td>
      </tr>
    <tr valign="top">
      <td colspan="2">
        <table cellpadding="0" cellspacing="0" align="center" class="contenttable">
          <tr valign="top">
            <td width="586" colspan="2"><div class="text"></div>	
              </td>
            <td width="282" rowspan="2" valign="bottom">&nbsp;</td>
            </tr>
          <tr valign="top">
            <td valign="bottom">&nbsp;</td>
            <td valign="bottom">&nbsp;</td>
            </tr>
          </table>
        </td>
      </tr>
  </table></div>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
