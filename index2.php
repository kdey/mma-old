<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Marlboro Montessori Academy | Day Care & Summer Camp - Monmouth County, NJ</title>
<link rel="stylesheet" href="mm.css" type="text/css">

</head>
<body style="font-weight:bold; font-family:'Arial';">
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/script.js"></script>
		

<div id="container">
 <a href="https://www.facebook.com/MarlboroMontessoriAcademy?ref=hl" title="Join Us On Facebook"><img src="http://www.facebookicons.net/images/76.png" alt="Icon" width="30px" height="30px" /></a>
</div>
<div align="center" itemscope itemtype="http://schema.org/Preschool">
<?php include("inc/header2.inc"); ?>
<div class="wrapper">

	<div class="content">
	 <div class="animation_area">
		<div id="header_ani">
	<!-- jQuery handles to place the header background images -->
	<div id="headerimgs">
		<div id="headerimg1" class="headerimg"></div>
		<div id="headerimg2" class="headerimg"></div>
	</div>

	<div id="headernav-outer">
		<div id="headernav">
			<div id="back" class="btn"></div>
			<div id="control" class="btn"></div>
			<div id="next" class="btn"></div>
		</div>
	</div>

</div>
		
		</div>
	
		<div class="hometext">
			<div class="hometext_top">
				<h1 style="text-align:center; line-height:40px;">Welcome to <span itemprop="name"> Marlboro Montessori Academy</span>:<br /> School, Summer Camp &amp; Day Care</h1>
			</div>
		
		<div class="hometext_bottom">
		
			<div class="hometext_bottom_left">
				<?php include("text/mm_home.txt"); ?><br/><br/>
				Featuring<span itemprop="rating"> 5 star ratings</span> in <a href="http://www.greatschools.org/new-jersey/wickatunk/3065-Marlboro-Montessori-Academy/" style="color:#FFFFFF; text-decoration:underline; font-family:sans-serif,Arial, Helvetica; font-size:14px;" target="new">GreatSchools.org</a>, Marlboro Montessori Academy is one of the most renowned private schools in the area.
				<br/><br/>
				<span itemprop="makesoffer" itemscope itemtype="http://schema.org/MakesOffer">
				Our sensational <span itemprop="makesoffer">Day Camp and Summer Camp for kids </span> offers a complete range of <span itemprop="makesoffer">summer activities for kids</span>. In addition to an on-site, <span itemprop="makesoffer">in-ground swimming program with trained instructors</span> we are proud to introduce the <span itemprop="makesoffer">Children''s Theatre Workshop</span> this summer. Our activities provide a healthy balance of <span itemprop="makesoffer">education and time in the sun</span>. Children of all ages love to pretend and enter worlds of the imagination. Our <span itemprop="makesoffer">Nature Explore Program</span> offers an amazing<span itemprop="makesoffer"> natural outdoor classroom</span>. The <span itemprop="makesoffer">Kids Learning Caf� provides special time to explore <span itemprop="makesoffer">cooking creativity</span> come alive in our kid''s kitchen. A special section on the East side of the campus is also reserved for <span itemprop="makesoffer">bird observation</span>.<br/><br/></span>
				<span itemprop="contactpoint" itemscope itemtype="http://schema.org/ContactPoint">
				Call us for your summer camp reservation now <span itemprop="telephone">732.946.CAMP</span> or email is at <span itemprop="email"><a href=marlmont@aol.com>marlmont@aol.com</a></span>
				</span>
			</div>
			
		  </div>
		<p style="text-align:right;"><a title="Marlboro Montessori NJ School Newsletter" href="mma_newsletter.pdf" style="color:#F9F0CF; font-size:15px;" target="new">Mar 2013 School Newsletter</a></p>
		
		</div>
		<div id="home4ImgBlock">
			<a href="method.php"><img src="images/home_img1.jpg" border="0" alt="Montessori Curriculum"></a>
			<a href="learningcenter.php"><img src="images/home_img2.jpg" border="0" alt="Montessori Preschool"></a>
			<a href="advantage.php"><img src="images/home_img3.jpg" border="0" alt="Montessori Materials"></a>
			<a href="camp.php"><img src="images/home_img4.jpg" border="0" alt="Summer Camp"></a>
		</div>	
		
		<?php include("inc/footer.inc"); ?>
	</div>

</div>

</div>

        <!-- JavaScript includes -->
<!--<script src="js/jquery.js" type="text/javascript"></script>-->
<script type="text/javascript">
        $(document).ready(function() {

            $(".signin").click(function(e) {          
				e.preventDefault();
                $("fieldset#signin_menu").toggle();
				$(".signin").toggleClass("menu-open");
            });
			
			$("fieldset#signin_menu").mouseup(function() {
				return false
			});
			$(document).mouseup(function(e) {
				if($(e.target).parent("a.signin").length==0) {
					$(".signin").removeClass("menu-open");
					$("fieldset#signin_menu").hide();
				}
			});			
			
        });
</script>
<script src="js/jquery.tipsy.js" type="text/javascript"></script>
<script type='text/javascript'>
    $(function() {
	  $('#forgot_username_link').tipsy({gravity: 'w'});   
    });
  </script>
</body>
</html>
