<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Marlboro Montessori Academy</title>
</head>
<body>
<div align="center">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content">
	<img src="images/camp_topimga.jpg"  /><br>
	<div class="hometext">
	  <table width="100%" border="0" cellpadding="8" cellspacing="10">
	    <tr>
	      <td width="33%" align="center" valign="top" bgcolor="#FFFFFF" class="text"><a href="swima.php"><img src="images/buttonswim.jpg" width="259" height="50" alt="Swimming" longdesc="swim.php"></a><br>
	        Our camp has certified lifeguards with a beautiful inground pool etc. <a href="swima.php" class="text"><em>Click here to learn more...</em></a></td>
	      <td width="34%" align="center" valign="top" bgcolor="#FFFFFF" class="text"><a href="naturea.php"><img src="images/buttonnature.jpg" width="259" height="50" alt="Nature Walks" longdesc="nature.php"></a><br>
	        Marlboro Montessori Camp not only has swimming facilities, it also has beautiful grounds for nature, exploring the different insects etc. <a href="naturea.php" class="text"><em>Click here to learn more...</em></a></td>
	      <td width="33%" align="center" valign="top" bgcolor="#FFFFFF" class="text"><a href="leadershipcampa.php"><img src="images/buttonleadership.jpg" width="259" height="50" alt="Leadership Academy" longdesc="leadershipcamp.php"></a><br>
	        The older children in our camp discover innovative ways to spark their curiosity etc. <a href="leadershipcampa.php" class="text"><em>Click here to learn more...</em></a></td>
	      </tr>
	    </table>
	</div>
	<div id="home5ImgBlock"><img src="images/camp_img5.jpg" border="0" alt="Montessori Schools" usemap="#home_img5" alt="Montessori Preschool" style="border-style:none" />
		<map id="home_img5" name="home_img5">
		<area shape="rect" alt="Matawan NJ Montessori" coords="3,152,171,302" href="advantage.php" title="" />
		<area shape="rect" alt="Old Bridge NJ Montessori" coords="2,3,177,141" href="learningcenter.php" title="" />
		<area shape="default" nohref="nohref" alt="Monmouth County Montessori" />
		</map>
	</div>			
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
