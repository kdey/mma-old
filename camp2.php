<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Marlboro Montessori - Day Care & Summer Camp, Monmouth County New Jersey (NJ)</title>
<style type="text/css">
	#header_ani1 { width:903px; height:270px; z-index:9999; }
.headerimg { width:903px; height:270px; position:absolute; z-index:9999; }


/* CONTROLS */
.btn { height:32px; width:32px; float:left; cursor:pointer; }
#back { background-image:url("images/btn_back.png"); }
#next { background-image:url("images/btn_next.png"); }
#control { background-image:url("images/btn_pause.png"); }

/* HEADER HAVIGATION */
#headernav-outer { position:relative; top:230px; margin:0 auto; width:903px; }
#headernav { padding-left:773px; }
</style>

<script type="text/javascript" src="js/jqery.js"></script>
<script type="text/javascript" src="js/camp_script.js"></script>


</head>
<body>
<div align="center">
<?php include("inc/header.inc"); ?>
<div class="wrapper"><div class="content">

		<div id="header_ani1">
	<!-- jQuery handles to place the header background images -->
	<div id="headerimgs">
		<div id="headerimg1" class="headerimg"></div>
		<div id="headerimg2" class="headerimg"></div>
	</div>

	<div id="headernav-outer">
		<div id="headernav">
			<div id="back" class="btn"></div>
			<div id="control" class="btn"></div>
			<div id="next" class="btn"></div>
		</div>
	</div>

</div>


	<div class="hometext"><?php include("text/mm_camp.txt"); ?></div>
	<div id="home5ImgBlock">
		<a href="mind.php"><img src="images/camp_img1.jpg" border="0" alt="Mind Travels"></a>
		<a href="nature.php"><img src="images/camp_img2.jpg" border="0" alt="Nature Watch"></a>
		<a href="swim.php"><img src="images/camp_img3.jpg" border="0" alt="Swim School"></a>
		<a href="leadershipcamp.php"><img src="images/camp_img4.jpg" border="0" alt="Leadership Academy"></a>
		<img src="images/camp_img5.jpg" border="0" alt="Montessori Schools" usemap="#home_img5" alt="Montessori Preschool" style="border-style:none" />
		<map id="home_img5" name="home_img5">
		<area shape="rect" alt="Matawan NJ Montessori" coords="3,152,171,302" href="advantage.php" title="" />
		<area shape="rect" alt="Old Bridge NJ Montessori" coords="2,3,177,141" href="learningcenter.php" title="" />
		<area shape="default" nohref="nohref" alt="Monmouth County Montessori" />
		</map>
	</div>			
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
