<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?php include("inc/meta_css.inc"); ?>
<title>Children's Theatre at Marlboro Montessori Summer Camp, Morganville, NJ</title>
<meta name="description" content="Build confidence and self esteem with Children Theatre Performance and Arts at Marlboro Montessori in NJ with easy accessibility from Matawan, Manalapan, Freehold"/>
<meta name="keywords" content="Summer Camp new Jersey, Day Care Camp Freehold, Preschool marlboro, Manalapan Montessori, After Care, After School Morganville, Elementary Schools marlboro, Elementary Schools Wickatunk, New Jersey, Central New Jersey Montessori, Monmouth County, Children's Theatre at montessori NJ, Nature, Birds, Swimming"/>
</head>
<body>

<div align="center">
<?php include("inc/header.inc"); ?>
<div class="wrapper" itemscope itemtype="http://schema.org/Preschool"><div class="content"><div class="maincontent">

	<img src="images/hd_bg.jpg" alt="Children's Theatre at Marlboro Montessori"  />
	<table cellpadding="0" cellspacing="0" align="center" class="contenttable">
		<tr valign="top">
			<td width="430" style="padding:0 0 0 20px;">
				<img src="images/children_theatre.png"  class="hdrimg" alt="Children's Theatre Workshop"/><br/>
				<meta itemprop="name" content="Marlboro Montessori Academy"/>
<meta itemprop="description" content="Build confidence and self esteem with Children Theatre Performance and Arts"/>
				<div class="text" itemprop="makesoffer" itemscope itemtype="http://schema.org/MakesOffer">
				<h1 style="color:#333333; margin:0px 25px -20px 0; font-size:18px;text-align:left; line-height:22px; font-weight:300">New to camp this summer is the <span itemprop="makesoffer">Children's Theatre Workshop</span>.</h1><br/> Children of all ages love to pretend and enter worlds of the imagination. Our camp at Marlboro Montessori near <span itemprop="location">Manalapan</span> in <span itemprop="location">Morganville</span> will provide your child with a creative and fun journey into the multi-sensory world of live theatre. Campers will participate in a variety of dramatics, improvisations and theater exercises similar to those done in a professional troupe. Miss Jamian, children's theatre teacher as well as music and singing teacher will be with us to foster life skills through <span itemprop="makesoffer">stage skills. Self confidence</span> will soar for our campers this summer!</div>	
			</td>
			<td width="450" align="center"><img src="images/img1.jpg"  class="hdrimg" alt="Summer Activities for children"/><br/></td>
			
			<tr>
			
			<td width="450" align="center"><img src="images/img4.jpg"  class="hdrimg" alt="Scavenger Hunts at Marlboro Montessori near Manalapan"/><br/></td>
			<td width="430" style="padding:0 0 0 20px;" valign="top"><div class="withSectionBorder">
				<img src="images/mind_hdr2.gif"  class="hdrimg" style="margin:5px;" alt="Sumeer camp for children with easy accessibility from Matawan, Manalapan and Freehold"/>
				<div class="text" style="margin-left:30px;"><?php include("text/mmc_mind2.txt"); ?></div>		
			</div></td>
			
			</tr>
			
		</tr>
		
		<!--<tr>
			<td width="903" align="center" colspan="2"><img src="images/photoframe.png"  class="hdrimg"/><br/></td>
			
			
		</tr>-->
	</table>
	
	
	</div><br/>
	<img src="images/photoframe.png"  class="hdrimg" alt="Summer camp Memory"/>
	<?php include("inc/footer.inc"); ?>
</div></div>
<br>
</div>
</body>
</html>
